Docker-Nagios
=============

## Synopsis

Docker image for [Nagios](https://www.nagios.com/), forked from [this project](https://hub.docker.com/r/jasonrivers/nagios/). This image is primarily intended to be used with our [Nagios Docker Configs](https://bitbucket.org/judgegroup/nagios-docker-configs)

This image provides Nagios Core 4.3.4 running on Ubuntu 16.04 LTS with NagiosGraph & NRPE.

### Configurations
Nagios Configuration lives in /opt/nagios/etc
NagiosGraph configuration lives in /opt/nagiosgraph/etc

## Building & Publishing

### Building with docker

Navigate to this project's root directory, and run

```
docker buld -t us.gcr.io/judge-161513/judge/nagios-docker:{version}
```
Where `{VERSION}` is the current [semver](http://semver.org) version number.

### Pushing to gcr

**Prerequisites:** An active [gcloud](https://cloud.google.com/sdk/gcloud/)
session for [Judge's GCP project](https://console.cloud.google.com/home/dashboard?project=judge-161513).

```
$ gcloud docker -- push us.gcr.io/judge-161513/judge/profile-image-service:{VERSION}
```

## Running and Deploying

### Running with Docker

To run with application on the machine where the Docker image was built:

```
docker run --name nagios4 -p {PORT}:80 \
us.gcr.io/judge-161513/judge/profile-image-service:{VERSION} \
-v /{NAGIOS_OPTS_DIRECTORY}/:/opt/nagios/etc/
```

..where `{PORT}` is the port number on which you'd like to expose the nagios UI,
`{VERSION}` is the version number of the image you built, above, and `{NAGIOS_OPTS_DIRECTORY}` is the directory containing the local configurations for the nagios instance.

#### Running an example instance

It's also possible to run with an example set of configurations:

```
docker run --name nagios4 -p {PORT}:80 \
us.gcr.io/judge-161513/judge/profile-image-service:{VERSION}
```

This provides a minimal setup which will only provide basic monitoring of localhost.
This is mainly useful just to make sure that it's possible to run nagios, and does little to provide meaningful monitoring.

#### Credentials

The default login for the web interface : `nagiosadmin` / `nagios`

### Extra Plugins

* Nagios nrpe [http://exchange.nagios.org/directory/Addons/Monitoring-Agents/NRPE--2D-Nagios-Remote-Plugin-Executor/details]
* Nagiosgraph [http://exchange.nagios.org/directory/Addons/Graphing-and-Trending/nagiosgraph/details]
* JR-Nagios-Plugins -  custom plugins from Jason Rivers [https://github.com/JasonRivers/nagios-plugins]
* WL-Nagios-Plugins -  custom plugins from William Leibzon [https://github.com/willixix/WL-NagiosPlugins]
* JE-Nagios-Plugins -  custom plugins from Justin Ellison [https://github.com/justintime/nagios-plugins]
